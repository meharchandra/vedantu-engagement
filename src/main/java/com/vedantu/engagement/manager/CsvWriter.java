package com.vedantu.engagement.manager;
import java.io.FileWriter; 
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random; 


public class CsvWriter { 

    private final String CRLF = "\r\n"; 
    private String delimiter = ","; 

    public void setDelimiter(String delimiter) { 
        this.delimiter = delimiter; 
    } 

    public void exportCsv(ArrayList<ArrayList<String>> rpt, String filename) { 
        try { 
            FileWriter writer = new FileWriter(filename,true);
            writer.append(delimiter+ CRLF);

            for (int i = 0; i < rpt.size(); i++) { 
                for (int j = 0; j < rpt.get(i).size(); j++) { 
                    writer.append( 
                            
                            rpt.get(i).get(j) 
                            ); 
                 
                    //Don't forget the delimiter 
                    if (j < rpt.get(i).size() - 1) { 
                        writer.append(delimiter); 
                    } 
                } 
                //Add delimiter and end of the line 
                if (i < rpt.size() - 1) { 
                    writer.append(delimiter + CRLF); 
                } 
            } 

            writer.flush(); 
            writer.close(); 
        } catch (IOException e) { 
            e.printStackTrace(); 
        } 
    }
    public void report(ArrayList<ArrayList<String>> rpt,String fileName)
    {
    	CsvWriter writer = new CsvWriter(); 
         
    //export them to a csv file 
    writer.exportCsv(rpt, fileName); 
    }
}