package com.vedantu.engagement.manager;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.apache.logging.log4j.Logger;

import com.vedantu.engagement.dao.serializers.EngagementDAO;
import com.vedantu.engagement.util.LogFactory;
import com.vedantu.engagement.viewobject.response.GetRequestsResponse;

@Service
public class EngagementManager {

	@Autowired
	public EngagementDAO EngagementDAO;

	@Autowired
	private LogFactory logfactory;

	@SuppressWarnings("static-access")
	private Logger logger = logfactory.getLogger(EngagementManager.class);

	public GetRequestsResponse getEngagement(Long id) throws Exception {
	//	InstaRequestDAO instaRequestDAO = new InstaRequestDAO();

	//	List<InstaProposalResponse> instaResponses = new ArrayList<InstaProposalResponse>();

		
	//	GetRequestsResponse getReqRes = new GetRequestsResponse(instaResponses);
	//	return getReqRes;
		Engag engage = new Engag();
		GetRequestsResponse getRequestsResponse = engage.getEngage(id);
		EngagementDAO.create(getRequestsResponse);
		return getRequestsResponse;
		
	}

}
