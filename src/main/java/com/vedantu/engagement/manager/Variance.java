package com.vedantu.engagement.manager;
import java.util.*;
public class Variance {

	public double slope(HashMap<Integer,HashMap<String,ArrayList<WB>>> pages, String tid)
	{
		ArrayList<Double> final_variance = new ArrayList<Double>();
		for (Map.Entry<Integer, HashMap<String, ArrayList<WB>>> page_num_entry : pages.entrySet()) 
		{
			Integer page_num = page_num_entry.getKey();
			System.out.println("page_num "+page_num);
			ArrayList<Double> variance = new ArrayList<Double>();

			for (Map.Entry<String, ArrayList<WB>> userId : page_num_entry.getValue().entrySet())
			{
				if(userId.getKey().equals(tid))
				{	
					ArrayList<WB> white_board = new ArrayList<WB>();
					white_board = userId.getValue();
					for(int i =0 ;i < white_board.size();i++)
					{
						if(!(white_board.get(i).type.equals("CREATEBOARD")) && !(white_board.get(i).type.equals("CHANGEBOARD")) && !(white_board.get(i).type.equals("REDRAWALL")))
						{
							int num_points = white_board.get(i).dat.points.length;
							ArrayList<Double> angle = new ArrayList<Double>();
							double avg = 0;
							double sum = 0;
							for(int m =0 ;m <num_points-1 ;m++)
							{
								if(white_board.get(i).dat.points[m][0] > 0 &&  white_board.get(i).dat.points[m][1] > 0 && white_board.get(i).dat.points[m][0] <= 902 && white_board.get(i).dat.points[m][1] <= 600 && white_board.get(i).dat.points[m+1][0] > 0 &&  white_board.get(i).dat.points[m+1][1] > 0 && white_board.get(i).dat.points[m+1][0] <= 902 && white_board.get(i).dat.points[m+1][1] <= 600 )
								{
									double x1 = white_board.get(i).dat.points[m][0];
									double y1 = white_board.get(i).dat.points[m][1];
									double x2 = white_board.get(i).dat.points[m+1][0];
									double y2 = white_board.get(i).dat.points[m+1][1];
									double angle1 = Math.atan2(y1, x1)*180/Math.PI;
									double angle2 = Math.atan2(y2, x2)*180/Math.PI;
									double delta = angle2- angle1;
									if(delta > 0)
										angle.add(delta);
									else
									{
										delta = -1 * delta;
										angle.add(-1* delta);
									}
								}
							}
							for(int m = 0 ;m< angle.size()-1; m++)
							{
								double consecutive = 0;
								consecutive = angle.get(m+1)-angle.get(m);
								angle.set(m,consecutive);
								avg+= consecutive;
								sum+= consecutive*consecutive;
							}
							if(angle.size() > 1)
							{
								sum/= (angle.size()-1);
								avg/= (angle.size()-1);
								avg = avg*avg;
								double var = sum - avg; //variance of difference of slopes
								variance.add(var);
							}
						}	
					}
				}
			}
			double avg_var = 0;
			for(int i =0; i< variance.size(); i++)
			{
				avg_var+= variance.get(i);
			}
			if(variance.size() > 0)
			{
				avg_var/= variance.size();
				final_variance.add(avg_var);
				System.out.println("variance of the page = "+ avg_var);
			}
		}
		double final_var = 0;
		for(int i =0 ;i<final_variance.size();i++)
		{
			final_var+= final_variance.get(i);
			System.out.println(final_variance.get(i));
		}
		if(final_variance.size() > 0)
		{
			final_var/= final_variance.size();
		}
		return final_var;
	}
}
