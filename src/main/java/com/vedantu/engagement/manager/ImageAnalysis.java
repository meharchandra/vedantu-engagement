package com.vedantu.engagement.manager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.*;
public class ImageAnalysis extends Analysis
{
	int img_vicinity_count;
	int img_count;
	
	ImageAnalysis()
	{
		img_vicinity_count = 0;
		img_count = 0;
	}
	
	public ArrayList<Long> image_frequency(HashMap<Integer,HashMap<String,ArrayList<WB>>> pages, long start, long end, String tid, String sid)
	{
		ArrayList<Long> image_time = new ArrayList<Long>();
		for (Map.Entry<Integer, HashMap<String, ArrayList<WB>>> page_num_entry : pages.entrySet()) 
		{
			Integer page_num = page_num_entry.getKey();
			for (Map.Entry<String, ArrayList<WB>> userId : page_num_entry.getValue().entrySet())
			{
				ArrayList<WB> white_board = new ArrayList<WB>();
				white_board = userId.getValue();
				for(int i =0 ;i < white_board.size();i++)
				{
					if(white_board.get(i).dat.t.equals("IMAGE"))
					{
						image_vicinity(white_board.get(i), pages, page_num, tid, sid);
						long cur = white_board.get(i).serverTime;
						image_time.add(cur);
						img_count++;
					}
				}
			}
		}
		
		double total_time = (end-start)*1.0/1000;
		double buffer_time = total_time/6;
		for(int i=0 ; i<image_time.size()-1;i++)
		{
			if((image_time.get(i+1)-image_time.get(i))*1.0/1000 > buffer_time)
			{	
				System.out.println("You could use an image here at server time " + image_time.get(i));
			}
		}
		return image_time;
	}
	public void image_vicinity(WB wb, HashMap<Integer,HashMap<String,ArrayList<WB>>> pages, Integer page_number, String tid, String sid)
	{
		int x1,y1,x2,y2;
		int distance = 10;
		x1 = wb.dat.points[0][0]; x2 = wb.dat.points[1][0]; y1 = wb.dat.points[0][1]; y2 = wb.dat.points[1][1];
		if((x1 > x2) && (y1 > y2))
		{
			int temp;
			temp = x1;x1=x2;x2=temp;
			temp = y1;y1=y2;y2=temp;
		}
		ArrayList<WB> white_board = pages.get(page_number).get(tid);
		if(white_board == null)
			return;
		for(int i =0 ;i < white_board.size(); i++)
		{
			int num_points = (white_board.get(i).dat.points!=null)?white_board.get(i).dat.points.length:0;
			Data d = white_board.get(i).dat;
			for(int m =0; m <num_points ; m++)
			{
				int temp_x, temp_y;
				temp_x = d.points[m][0];
				temp_y = d.points[m][1];
				if((temp_x < x1  && temp_x > x1-distance) || (temp_x > x2 && temp_x < x2 + distance) || (temp_y < y1 && temp_y > y1-distance) ||( temp_y > y2 && temp_y < y2+distance))
				{
					img_vicinity_count++;
				}
				else if((temp_x > x1 && temp_x <x2) && (temp_y > y1 && temp_y < y2))
				{
					img_vicinity_count++;
				}
			}
		}
	}	
}
