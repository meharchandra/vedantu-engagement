package com.vedantu.engagement.manager;
import java.util.*;

public class Scoring_1 {

	int[] ans;
	public int[] Score(ArrayList<String> param, double[] val)
	{
		int scr=0;
		int flg=0;
		int[] ret = new int[2];
		String temp = param.get(14);
		param.remove(14);
		double sh = Double.parseDouble(param.get(2));
		double cc = Double.parseDouble(param.get(4));
		double twl =  Double.parseDouble(param.get(5));
		double wbsu = Double.parseDouble(param.get(7));
		double twt = Double.parseDouble(param.get(8));
		double wit = Double.parseDouble(param.get(10));
		double nwb = Double.parseDouble(param.get(11));
		double inter = Double.parseDouble(param.get(15));
		double str = Double.parseDouble(param.get(19));
		double st = Double.parseDouble(param.get(21));
		param.add(14,temp);
		if(sh<val[0]){scr +=0;flg+=1;}
		else if(sh<val[1])scr +=1;
		else scr+=2;
		
		if(cc<val[2]){scr +=0;flg+=1;}
		else if(cc<val[3])scr +=1;
		else scr+=2;
		
		if(twl<val[4]){scr +=0;flg+=1;}
		else if(twl<val[5])scr +=1;
		else scr+=2;
		
		if(wbsu<val[6]){scr +=0;flg+=1;}
		else if(wbsu<val[7])scr +=1;
		else scr+=2;
		
		if(twt<val[8]){scr +=0;flg+=1;}
		else if(twt<val[9])scr +=1;
		else scr+=2;
		
		if(wit>val[10]){scr +=0;flg+=1;}
		else if(wit>val[11])scr +=1;
		else scr+=2;
					
		if(nwb<val[12]){scr +=0;flg+=1;}
		else if(nwb<val[13])scr +=1;
		else scr+=2;
		
		if(inter<val[14]){scr +=0;flg+=1;}
		else if(inter<val[15])scr +=1;
		else scr+=2;
		
		if(str<val[16]){scr +=0;flg+=1;}
		else if(str<val[17])scr +=1;
		else scr+=2;
		
		if(st<val[18]){scr +=0;flg+=1;}
		else if(st<val[19])scr +=1;
		else scr+=2;
		
		ret[0]=scr;
		ret[1]=flg;
		return ret;
	}
	public Scoring_1(ArrayList<String> param)
	{
		String score;
		
		double[] val = new double[20]; 
		if(param.get(1).equals("Physics"))
		{
			val[0]=5.0;
			val[1]=12.0;
			val[2]=8.5;
			val[3]=30.0;
			val[4]=20000;
			val[5]=150000;
			val[6]=48;
			val[7]=68;
			val[8]=220;
			val[9]=940;
			val[10]=85;
			val[11]=72;
			val[12]=8.0;
			val[13]=16.0;
			val[14]=85;
			val[15]=270;
			val[16]=0.04;
			val[17]=0.20;
			val[18]=70;
			val[19]=420;
			
		}
		else if(param.get(1).equals("Chemistry"))
		{
			val[0]=3.5;
			val[1]=10.0;
			val[2]=8.5;
			val[3]=20.0;
			val[4]=20000;
			val[5]=150000;
			val[6]=48;
			val[7]=68;
			val[8]=350;
			val[9]=1250;
			val[10]=85;
			val[11]=72;
			val[12]=8.0;
			val[13]=16.0;
			val[14]=70;
			val[15]=230;
			val[16]=0.04;
			val[17]=0.20;
			val[18]=75;
			val[19]=550;
		}
		else if(param.get(1).equals("Mathematics"))
		{
			val[0]=5.0;
			val[1]=13.0;
			val[2]=8.5;
			val[3]=30.0;
			val[4]=20000;
			val[5]=115000;
			val[6]=48;
			val[7]=68;
			val[8]=220;
			val[9]=940;
			val[10]=85;
			val[11]=72;
			val[12]=9.0;
			val[13]=16.0;
			val[14]=70;
			val[15]=230;
			val[16]=0.04;
			val[17]=0.30;
			val[18]=26;
			val[19]=350;
		}
		else
		{
			val[0]=3.0;
			val[1]=9.0;
			val[2]=8.5;
			val[3]=20.0;
			val[4]=15000;
			val[5]=140000;
			val[6]=48;
			val[7]=68;
			val[8]=12;
			val[9]=890;
			val[10]=88;
			val[11]=72;
			val[12]=5.0;
			val[13]=11.0;
			val[14]=70;
			val[15]=200;
			val[16]=0.04;
			val[17]=0.15;
			val[18]=60;
			val[19]=515;
		}
		ans = Score(param,val);

		//return ans;
	}
}
