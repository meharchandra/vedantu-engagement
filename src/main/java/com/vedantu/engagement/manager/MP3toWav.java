package com.vedantu.engagement.manager;
import it.sauronsoftware.jave.AudioAttributes;
import it.sauronsoftware.jave.Encoder;
import it.sauronsoftware.jave.EncoderException;
import it.sauronsoftware.jave.EncodingAttributes;
import it.sauronsoftware.jave.FFMPEGLocator;
import it.sauronsoftware.jave.InputFormatException;
import it.sauronsoftware.jave.MultimediaInfo;

import java.io.File;

public class MP3toWav
{

	public MP3toWav(String input,String output) {
		
		File source = new File(input);
		File target = new File(output);
		AudioAttributes audio	= new AudioAttributes();
		audio.setCodec("pcm_s16le");
		audio.setChannels(1);
	//	audio.setBitRate(new Integer(128000));
		audio.setChannels(new Integer(1));
		
	//	audio.setSamplingRate(new Integer(44100));
		EncodingAttributes attrs = new EncodingAttributes();
		attrs.setFormat("wav");
		attrs.setAudioAttributes(audio);
		FFMPEGLocator locator = new FFMPEGLocator() {
			
			@Override
			protected String getFFMPEGExecutablePath() {
				return "/Users/meharchandra/Downloads/SnowLeopard_Lion_Mountain_Lion_Mavericks_12.12.2015/ffmpeg";
			}
		};
		Encoder encoder = new Encoder(locator);
		try {
			encoder.encode(source, target, attrs);
		} catch (Exception e) {
		}
	}
	}