package com.vedantu.engagement.manager;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.net.URL;

import ca.pjer.ekmeans.EKmeans;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import java.util.*;

class WB //White Board
{
	String _id;
	String context;
	String socketId;
	int sentTime;
	String sessionId;
	long serverTime;
	String userId;
	long lastUpdated;
	long creationTime;
	String data;
	String type;
	long id;
	int __v;
	Data dat;
}

class Data
{
	String t;
	String c;	
	int s;
	int tm;				//time map
	String id;
	int bw;             //width
	int bh;				//height
	String bi;			//page number
	String p;
	String st;
	String en;
	String boardId;
	int index;
	long _timeStamp;
	List<String> pstr = new ArrayList<String>();
	List<String> pstr1 = new ArrayList<String>();
	int[][] points;


	public void strToInt()
	{
		if(p != null)
		{
			pstr = Arrays.asList(p.split(";"));
			String[][] pint = new String[pstr.size()][2];
			points = new int[pstr.size()][2];
			for(int i=0;i< pstr.size();i++)
			{

				pint[i] = pstr.get(i).split(",");
				points[i][0] = (int)Double.parseDouble(pint[i][0]);
				points[i][1] = (int)Double.parseDouble(pint[i][1]);

			}

		}
		if(p == null  && st != null)
		{
			pstr = Arrays.asList(st.split(";"));
			String[][] pint = new String[2][2];
			points = new int[2][2];

			pint[0] = pstr.get(0).split(",");
			points[0][0] = (int)Double.parseDouble(pint[0][0]);
			points[0][1] = (int)Double.parseDouble(pint[0][1]);

			pstr1 = Arrays.asList(en.split(";"));
			pint[1] = pstr1.get(0).split(",");
			points[1][0] = (int)Double.parseDouble(pint[1][0]);
			points[1][1] = (int)Double.parseDouble(pint[1][1]);
		}
	}
}
class Proximity 
{
	public ArrayList<Double> write(HashMap<Integer,HashMap<String,ArrayList<WB>>> pages, int num_pages, String tid, String sid) throws IOException
	{
		ArrayList<Double> final_den = new ArrayList<Double>();
		ArrayList<Double> final_space = new ArrayList<Double>();
		for (Map.Entry<Integer, HashMap<String, ArrayList<WB>>> page_num_entry : pages.entrySet()) 
		{
			int [][]grid = new int[902][600];
			for(int i =0; i< 902 ;i++)
			{
				for(int j =0 ;j < 600 ;j++)
				{
					grid[i][j] = 0;
				}
			}
			double[][] points = new double[1000000][2];
			int p_index = 0;
			Integer page_num = page_num_entry.getKey();
			System.out.println("page_num "+page_num);
			double area = 0;
			double density = 0;
			ArrayList<Double> den = new ArrayList<Double>();

			int temp_points = 0;
			int avg_pixel = 0;
			for (Map.Entry<String, ArrayList<WB>> userId : page_num_entry.getValue().entrySet())
			{
				if(userId.getKey().equals(tid))
				{	
					ArrayList<WB> white_board = new ArrayList<WB>();
					white_board = userId.getValue();
					for(int i =0 ;i < white_board.size();i++)
					{
						if(!(white_board.get(i).type.equals("CREATEBOARD")) && !(white_board.get(i).type.equals("CHANGEBOARD")) && !(white_board.get(i).type.equals("REDRAWALL")))
						{
							int num_points = white_board.get(i).dat.points.length;

							for(int m =0 ;m <num_points ;m++)
							{
								if(white_board.get(i).dat.points[m][0] > 0 &&  white_board.get(i).dat.points[m][1] > 0 && white_board.get(i).dat.points[m][0] <= 902 && white_board.get(i).dat.points[m][1] <= 600 )
								{
									try
									{
										avg_pixel += white_board.get(i).dat.s;
									}
									catch(Exception e)
									{

									}
									if(grid[white_board.get(i).dat.points[m][0]-1][white_board.get(i).dat.points[m][1]-1] == 0)
									{
										temp_points++;
										grid[white_board.get(i).dat.points[m][0]-1][white_board.get(i).dat.points[m][1]-1] = 1;
										points[p_index][0] = white_board.get(i).dat.points[m][0];
										points[p_index++][1] = white_board.get(i).dat.points[m][1];
									}

								}
							}
						}
					}
				}
			}
			Empty empty = new Empty();
			double space = empty.empty_space(grid,902,600);
			final_space.add(space);
			System.out.println(" temp_points ="+temp_points);
			int k = 10; // the number of clusters
			if(temp_points <= k*10)
			{
				num_pages-- ;
				continue;
			}
			avg_pixel/= temp_points;
			if(avg_pixel <= 2)
				avg_pixel = 2;
			int n = temp_points; // the number of points to cluster
			double[][] centroids = new double[k][2];
			int div = temp_points/k;

			for (int i = 0; i < k ; i++) {
				centroids[i][0] = points[(div-i)*k-1][0];
				centroids[i][1] = points[(div-i)*k-1][1];
			}
			EKmeans eKmeans = new EKmeans(centroids, points);
			eKmeans.setIteration(1000);
			eKmeans.setDistanceFunction(EKmeans.EUCLIDEAN_DISTANCE_FUNCTION);
			eKmeans.run();
			int[] assignments = eKmeans.getAssignments();
			ArrayList<ArrayList<Integer>> clusters = new ArrayList<ArrayList<Integer>>();
			for(int i =0 ;i<k ;i++)
			{
				clusters.add(new ArrayList<Integer>());
			}

			for (int i = 0; i < n; i++) 
			{
				clusters.get(assignments[i]).add(i);
			}	
			for(int i =0 ;i< k ; i++)
			{
				double x_avg =0;
				double y_avg =0;
				double radius = Integer.MIN_VALUE;
				if(clusters.get(i).size() < 10)
				{
					continue;
				}

				for(int j =0 ; j< clusters.get(i).size() ;j++)
				{
					int index = clusters.get(i).get(j);
					y_avg += points[index][1];
					x_avg += points[index][0];
				}
				x_avg/=clusters.get(i).size();
				y_avg/=clusters.get(i).size();
				for(int j =0 ; j < clusters.get(i).size();j++)
				{
					int index = clusters.get(i).get(j);
					double x = (x_avg - points[index][0]);
					double y = (y_avg - points[index][1]);
					double square = x*x+y*y;

					if(square > radius)
						radius = square; //radius is already square of distance (r^2)
				}
				if(radius > 0)
				{
					area = 3.14*radius;
					density = clusters.get(i).size()/area;
					den.add(density);
				}
				else
				{					
					continue;
				}
			}
			// We have the density of all the clusters in a single page 
			double temp_density =0;
			for(int i =0 ;i <den.size() ;i ++)
			{
				if(den.get(i)*1000 > 75)
					System.out.println("Density of Cluster " + i + " is "+ den.get(i)*1000 );
			}
			for(int i =0 ;i <den.size();i++)
			{
				temp_density+= den.get(i);
			}
			if(den.size() == 0)
			{
				continue;
			}
			temp_density/= den.size();
			temp_density*= 1000;

//------------- for taking median of the data instead of mean ----------------------------
			/*	
		 	Collections.sort(den);
			for(int i =0 ;i <den.size() ;i ++)
			{
				System.out.println("Density of Cluster " + i + "is "+ den.get(i)*1000 );
			}

			if(den.size() % 2 == 0)
			{
				temp_density = (den.get(den.size()/2) + den.get(den.size()/2-1))/2;
			}
			else
			{
				temp_density = den.get(den.size()/2);
			}
			 */
//----------------------------------------------------------------------------------------			
			final_den.add(temp_density);
			System.out.println("density of the page "+temp_density);
			den.clear();
		}
		double avg = 0.0;
		for(int i = 0 ; i< final_space.size(); i++)
		{
			avg += final_space.get(i);
		}
		avg/= final_space.size();
		avg = avg*100.0/492;
		double final_density = 0;
		ArrayList<Double> values = new ArrayList<Double>();
		values.add(avg);
		Collections.sort(final_den);
		if(num_pages == 0)
		{
			values.add(final_density);
			return values;
			//return final_density;
		}
		else if(num_pages == 1)
		{
			values.add(final_den.get(0));
			return values;
			//return final_den.get(0);
		}

		if(num_pages % 2 ==0 && num_pages !=0)
		{
			final_density = (final_den.get(num_pages/2)+final_den.get(num_pages/2-1))/2;
		}
		else
		{
			if(num_pages !=0)
				final_density = final_den.get(num_pages/2);
		}
		System.out.println("avg blocks used  = "+avg );
		System.out.println("Density of the session is = "+final_density);
		
		values.add(final_density);
		return values;
	}

}

public class mehar_redesign {
	int index;
	String tid;
	String sid;
	String id;
	ImageAnalysis analysis = new ImageAnalysis();
	ArrayList<Long> image_time = new ArrayList<Long>();
	long start;
	long end;
	BufferedReader buf;
	Gson gson;
	Type collectionType;
	ArrayList<WB> wb;
	HashMap<Integer,HashMap<String,ArrayList<WB>>> pages;
	int [][]points_time_map;
	int[] use;
	ArrayList<Integer> time_map;
	double wb_silence;
	Proximity proximity;
	int num_pages=0;
	//--------------------------------------------------Initialization completed  -----------------------------------------------------------------
	public ArrayList<String> check(String args,String[] ids) throws FileNotFoundException,Exception {
		// ------------------------------------------------- Automation Part --------------------------------------------------------------------------		
		long startTime = System.nanoTime();
		tid = ids[0];
		sid =ids[1];
		
		// --------------------------------------------------getting data from url------------------------------------------------------------------- 		
		BufferedReader reader = null;
		BufferedWriter wr = null;
		PrintWriter writer = null;
		writer = new PrintWriter("mehar_data.json", "UTF-8");
		StringBuilder sb = new StringBuilder();
		JsonArray result;
		try {
			String SessionId = args;
			String urlString = "https://rtc.vedantu.com/fetchsessiondata?context=whiteboard&sessionId="+SessionId +"&size=100000&start=0";
			URL url = new URL(urlString);
			reader = new BufferedReader(new InputStreamReader(url.openStream()));

			int read;
			char[] chars = new char[1];
			while ((read = reader.read(chars)) != -1)
			{	
				sb.append(chars);				
			}

			/*			String inputLine;
	        while ((inputLine = reader.readLine()) != null) 
	            sb.append(inputLine);
			 */	            
			String str = sb.toString();

			JsonParser parser = new JsonParser();
			JsonObject obj = parser.parse(str).getAsJsonObject();
			result = obj.getAsJsonArray("result");

		} finally {
			if (reader != null)
				reader.close();
		}
		System.out.println("data fetched form url");
		//---------------------------------------------------------------------------------------------------------------------------------------------		
		gson = new GsonBuilder().create();
		collectionType = new TypeToken<ArrayList<WB>>() {
		}.getType();
		
		wb = gson.fromJson(result, collectionType);
		for(int i =wb.size()-1; i > 0;i--)
		{

			if(wb.get(i).data.startsWith("{"))
			{
				wb.get(i).dat = gson.fromJson(wb.get(i).data,Data.class);

				wb.get(i).dat.strToInt();
			}
			else {
				wb.remove(i);
			}
		}
		index = 0;
		id ="";

		pages = new HashMap<Integer,HashMap<String,ArrayList<WB>>>();

		long audio_start;// = Long.parseLong(ids[4]);
		if(ids[4] != null)
			audio_start = Long.parseLong(ids[4]);
		else 
			audio_start =0;
		long wb_start;// = wb.get(wb.size()-1).serverTime;
		if(wb.size()-1 > 5)wb_start = wb.get(wb.size()-1).serverTime;
		else wb_start=0;
		long delta = audio_start - wb_start;
		System.out.println("detla = " + delta);
		long active_duration = 0;
		if(wb.size()>5)active_duration= (wb.get(0).serverTime - wb.get(wb.size()-1).serverTime);
		for(int i = wb.size()-1;i>0;i--)
		{    
			if(wb.get(i).type.equals("CHANGEBOARD") || wb.get(i).type.equals("CREATEBOARD"))
			{
				continue;
			}

			try
			{
				index = Integer.parseInt(wb.get(i).dat.bi);
			}
			catch(java.lang.NumberFormatException e)
			{
				continue;
			}

			if(wb.get(i).type.equals("REMOVEALL"))
			{
				pages.remove(index);
				continue;
			}

			id = wb.get(i).userId;
			if(pages.get(index)==null){
				pages.put(index, new HashMap<String, ArrayList<WB>>());
			}
			if(pages.get(index).get(id)==null)
			{
				pages.get(index).put(id, new ArrayList<WB>());
			}

			pages.get(index).get(id).add(wb.get(i));
		}
		// ---------------------------------------------------------- csv ---------------------------------------------------------------------------		
		ArrayList<String> testcsv = new ArrayList<>(); 
		num_pages = pages.size();
		System.out.println("Analysis Started");	
		testcsv = analysis.page_analysis(pages, tid, sid);
		try
		{
			start = wb.get(wb.size()-1).serverTime;
			end = wb.get(0).serverTime;
		}
		catch(java.lang.ArrayIndexOutOfBoundsException e)
		{
			ArrayList<String> empty = new ArrayList<String>();
			return empty;
		}

		image_time = analysis.image_frequency(pages, start, end, tid, sid);
		double annotation = 0;
		if(analysis.img_count > 0)
			annotation = analysis.img_vicinity_count*1.00/analysis.img_count;
		System.out.println("annotation = "+ annotation);

		time_map = analysis.Time_Map(pages, tid, sid, start, end);
		points_time_map = analysis.Count_Time_Map(pages, tid, sid, start, end);
		use = analysis.UserWBUsage(time_map);
		System.out.println("Total White Board Time "+ time_map.size());
		testcsv.add(Integer.toString(time_map.size()));

		System.out.println("Teacher White Board Time "+ use[0]);
		testcsv.add(Integer.toString(use[0]));

		System.out.println("Student White Board Time "+ use[1]);
		testcsv.add(Integer.toString(use[1]));

		wb_silence = analysis.WhiteBoardSilence(time_map);
		System.out.println("White Board Silence Percentage " + wb_silence*100 );
		testcsv.add(Double.toString(wb_silence*100));



		proximity = new Proximity();
		System.out.println("num_pages ="+ num_pages);
		testcsv.add(Integer.toString(num_pages));
		ArrayList<Double> values = proximity.write(pages, num_pages, tid, sid);
		
		Variance variance = new Variance();
		double var = variance.slope(pages, tid);
		
		
		testcsv.add(Double.toString(values.get(1)));
		System.out.println("teacher id " + tid );
		System.out.println("student id "+ sid);

		long endTime = System.nanoTime();
		testcsv.add(0,Long.toString(active_duration/1000));
		testcsv.add(1,Long.toString(delta/1000));
		int shapes = Integer.parseInt(testcsv.get(2))+Integer.parseInt(testcsv.get(3))+Integer.parseInt(testcsv.get(4))+Integer.parseInt(testcsv.get(5));
		System.out.println("shapes = "+ shapes);
		testcsv.add(Integer.toString(shapes));
		testcsv.add(testcsv.get(6));
		testcsv.add(testcsv.get(7));
		testcsv.add(testcsv.get(8));
		double space_usage = Double.parseDouble(testcsv.get(10))*100.0;
		testcsv.add(Double.toString(space_usage));
		testcsv.add(testcsv.get(12));
		testcsv.add(testcsv.get(13));
		testcsv.add(testcsv.get(14));
		testcsv.add(testcsv.get(15));
		testcsv.add(testcsv.get(16));
		if(testcsv.get(5).equals("0"))
			testcsv.add("0");
		else
			testcsv.add("1");
		testcsv.add(Double.toString(var));
		testcsv.add(Double.toString(annotation));
		/*for(int i =0 ;i< testcsv.size(); i++)
		{
			System.out.println("i = "+i + " testcsv.get("+i+") "+testcsv.get(i));
		}
		ArrayList<ArrayList<String>> wb_temp_out = new ArrayList<ArrayList<String>>();
		wb_temp_out.add(testcsv);
		CsvWriter csv = new CsvWriter();
		csv.report(wb_temp_out,"whiteboard.csv");
		*/
		return testcsv;

	}
}
