package com.vedantu.engagement.manager;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;

public class Wav2Text 
{
	
    private BufferedInputStream bfread;
    private byte[] buffer;
    private int temp;
    public ArrayList<Double> x=new ArrayList<Double>();
    private double normalizationFactor = 15000;

    
    
    public Wav2Text(String input) throws IOException{
        try {
        	
        	//Read wav file
	        //File file = new File(fpath);
            bfread = new BufferedInputStream(new FileInputStream(input));
            
            buffer = new byte[2];
            
            //this.convert();
            
        } catch (Exception e) {
        	System.err.println(e.getMessage());
        }
    }
    
    /**
     * Method to start the conversion process
     */
    public void convert(){
        try {
            printHeader();
            printData();
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }

    /**
     * Method to record the data section of a WAV file. In a WAV format, the higher byte 
     * comes second. So, the higher byte is read second into <code>buffer[1]</code>. The lower byte
     * which is read first is then masked with <code>0x000000FF</code> for the lower
     * eight bits(removing the sign bit). The 16-bit data value is constructed by 
     * shifting the higher byte by 8 bits and then ORing with the lower byte.
     */
    private void printData(){
        try {
            while (bfread.read(buffer) >= 0) {
                temp = 0;
                temp = buffer[1];
                temp <<= 8;
                temp |= (0x000000FF & buffer[0]);
                x.add(new Double(temp));
            }
        //    process();
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }


    /**
     * Performs DC-shift and normalization on the speech sample.
     */
    private void process(){
        double dc = 0;
        
        // Perform DC Shift
        for(int i=0;i<x.size();i++)
            dc += ((Double) x.get(i)).doubleValue();
        dc = dc/x.size();
        
        if(dc != 0){
            for(int i=0;i<x.size();i++)
                x.set(i,((Double) x.get(i)) - dc);
        }
        
        // Normalize
        normalize();
        
       
    }
    
    /**
     * Method to normalize the input sample values before processing for LPC
     */
    private void normalize(){
        double max = Math.abs(((Double) x.get(0)).doubleValue());
        double tmp;
        for(int i=1;i<x.size();i++){
            tmp = Math.abs(((Double)x.get(i)).doubleValue());
            if(max < tmp)
                max = tmp;
        }
        for(int i=0;i<x.size();i++){
            tmp = ((Double) x.get(i)).doubleValue();
            tmp = (tmp/max) * normalizationFactor;
            x.set(i,tmp);
        }
    }
    
    /**
     * Skip the header of the WAV file. The method skips the first 44 bytes of 
     * the WAV file which is the header.
     * @throws java.io.IOException
     */
    private void printHeader() throws IOException{
        bfread.skip(44);
    }
}

