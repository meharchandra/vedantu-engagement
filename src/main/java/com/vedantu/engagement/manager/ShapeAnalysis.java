package com.vedantu.engagement.manager;
import java.util.*;
import java.util.Map.Entry;
public class ShapeAnalysis {
		
		int rect_count;
		int ellip_count;
		int rtrian_count;
		int trian_count;
		
		ShapeAnalysis(int rc, int ec, int rtc, int tc)
		{
			rect_count = rc;
			ellip_count = ec;
			rtrian_count = rtc;
			trian_count = tc;
		}
		public double MinDistance(double x, double y, double z)
		{
			if(x <y)
			{
				if(x < z)
				{
					return x;
				}
				else
				{
					return z;
				}
			}
			else
			{
				if(y < z)
					return y;
				else
					return z;
			}
		}
		public double FindVertexDistance(int x1, int y1, int x2,int y2)
		{
			double t1 = x1-x2;
			double t2 = y1-y2;
			return Math.sqrt((t1*t1)+(t2*t2));
		}
		public void SolveEquation(int x1, int y1, int x2, int y2, int temp_x, int temp_y)
		{
			double m = (y2-y1)*1.0/(x2-x1);
			double k = temp_y+temp_x*1.0/m;
			double a,b,c,d,u,v;
			a=m;
			b=-1;
			c=1;
			d=m;
			u = m*x1-y1;
			v = m*k;
		/* a * x + b * y = u
			c * x + d * y = v
		*/	
		/*	def solve(a, b, c, d, u, v):
			    if abs(a) > abs(c):
			         f = u * c / a
			         g = b * c / a
			         y = (v - f) / (d - g)
			         return ((f - g * y) / c, y)
			    else
			         f = v * a / c
			         g = d * a / c
			         x = (u - f) / (b - g)
			         return (x, (f - g * x) / a)
		 */
			
		}
		public void FindMinDistance(int x1,int y1,int x2,int y2,int x3,int y3,int temp_x,int temp_y)
		{
			double distanceVertex,d1,d2,d3,DistanceSide;
			d1=FindVertexDistance(x1,y1,temp_x,temp_y);
		    d2=FindVertexDistance(x2,y2,temp_x,temp_y);
		    d3=FindVertexDistance(x3,y3,temp_x,temp_y);
		    distanceVertex = MinDistance(d1,d2,d3);
		    
		    SolveEquation(x1,y1,x2,y2,temp_x,temp_y);
		    SolveEquation(x1,y1,x3,y3,temp_x,temp_y);
		    SolveEquation(x2,y2,x3,y3,temp_x,temp_y);
		    
		}
		public void rect_vicinity(WB wb, HashMap<Integer,HashMap<String,ArrayList<WB>>> pages, Integer page_number)
		{
			int x1,y1,x2,y2;
			int distance = 10;
			x1 = wb.dat.points[0][0]; x2 = wb.dat.points[1][0]; y1 = wb.dat.points[0][1]; y2 = wb.dat.points[1][1];
			if((x1 > x2) && (y1 > y2))
			{
				int temp;
				temp = x1;x1=x2;x2=temp;
				temp = y1;y1=y2;y2=temp;
			}
			
			for(Map.Entry<String, ArrayList<WB>> userId :pages.get(page_number).entrySet() )
			{
				ArrayList<WB> white_board = new ArrayList<WB>();
				white_board = userId.getValue();
				for(int i =0 ;i < white_board.size(); i++)
				{
					int num_points = white_board.get(i).dat.points.length;
					Data d = white_board.get(i).dat;
					for(int m =0; m <num_points ; m++)
					{
						int temp_x, temp_y;
						temp_x = d.points[m][0];
						temp_y = d.points[m][1];
						if((temp_x < x1  && temp_x > x1-distance) || (temp_x > x2 && temp_x < x2 + distance) || (temp_y < y1 && temp_y > y1-distance) ||( temp_y > y2 && temp_y < y2+distance))
						{
							rect_count++;
						}
						else if((temp_x > x1 && temp_x <x2) && (temp_y > y1 && temp_y < y2))
						{
							rect_count++;
						}
					}
				}
			}

		}
		public void trian_vicinity(WB wb, HashMap<Integer,HashMap<String,ArrayList<WB>>> pages, Integer page_number)
		{
			int x1,y1,x3,y3,x2,y2;
			int distance = 10;
			x1 = wb.dat.points[0][0]; x3 = wb.dat.points[1][0]; y1 = wb.dat.points[0][1]; y3 = wb.dat.points[1][1];
			if((x1 > x3) && (y1 > y3))
			{
				int temp;
				temp = x1;x1=x3;x3=temp;
				temp = y1;y1=y3;y3=temp;
			}
			x2 = x1;
			y2 = y3;
			for(Map.Entry<String, ArrayList<WB>> userId :pages.get(page_number).entrySet() )
			{
				ArrayList<WB> white_board = new ArrayList<WB>();
				white_board = userId.getValue();
				for(int i =0 ;i < white_board.size(); i++)
				{
					int num_points = white_board.get(i).dat.points.length;
					Data d = white_board.get(i).dat;
					for(int m =0; m <num_points ; m++)
					{
						int temp_x, temp_y;
						temp_x = d.points[m][0];
						temp_y = d.points[m][1];
						FindMinDistance(x1,y1,x2,y2,x3,y3,temp_x,temp_y);
					}
				}
			}
			
		}
		public void shape_vicinity(HashMap<Integer,HashMap<String,ArrayList<WB>>> pages)
		{
			for (Map.Entry<Integer, HashMap<String, ArrayList<WB>>> page_num_entry : pages.entrySet()) 
			{
				Integer page_num = page_num_entry.getKey();
				for (Map.Entry<String, ArrayList<WB>> userId : page_num_entry.getValue().entrySet())
				{
					ArrayList<WB> white_board = new ArrayList<WB>();
					white_board = userId.getValue();
					for(int i =0 ;i < white_board.size();i++)
					{
						if(white_board.get(i).dat.t.equals("RECT"))
						{
							rect_vicinity(white_board.get(i), pages, page_num);
						}
						else if(white_board.get(i).dat.t.equals("RIGHTTRIANLE"))
						{
							trian_vicinity(white_board.get(i), pages, page_num);
						}
						else if(white_board.get(i).dat.t.equals("ELLIPSE"))
						{
					//		ellip_vicinity(white_board.get(i), pages, page_num);
						}
					}
				}
			}
		}
}
