package com.vedantu.engagement.manager;
import java.util.*;
public class Analysis
{
	int img;
	double useRatio;
	int tcnt;
	int scnt;
	ArrayList<Double> clum = new ArrayList<Double>();

	public  double writeLength(int x1,int x2,int y1,int y2)
	{
		int t1 = x2-x1;
		int t2 = y2-y1;
		return Math.sqrt((t1*t1)+(t2*t2));
	}
	public  int[] UserWBUsage(ArrayList<Integer> time_map)
	{
		int[] useRange = new int[2];
		useRange[0] = 0;
		useRange[1] = 0;

		for(int i =0 ;i <time_map.size();i++)
		{
			if(time_map.get(i) == 1)
			{
				useRange[0]++;
			}
			else if (time_map.get(i) == 2)
			{
				useRange[1]++;
			}
		}
		return useRange;
	}
	public  double WhiteBoardSilence(ArrayList<Integer> time_map)
	{
		int count =0;
		for(int i =0 ;i <time_map.size();i++)
		{
			if(time_map.get(i) == 0)
			{
				count++;
			}
		}
		return (1.0*count/time_map.size());		
	}
	public  int[][] Count_Time_Map(HashMap<Integer,HashMap<String,ArrayList<WB>>> pages, String tid, String sid,long start, long end)
	{
		int [][]points_time_map;
		long range = end-start;
		int size = (int)(range/1000+2);
		points_time_map = new int[size][2];
		for(int i =0;i< (int)((range/1000)+2);i++)
		{
			points_time_map[i][0]=0;
			points_time_map[i][1]=0;
		}
		for (Map.Entry<Integer, HashMap<String, ArrayList<WB>>> page_num_entry : pages.entrySet()) 
		{
			Integer page_num = page_num_entry.getKey();
			for (Map.Entry<String, ArrayList<WB>> userId : page_num_entry.getValue().entrySet())
			{
				ArrayList<WB> white_board = new ArrayList<WB>();
				white_board = userId.getValue();
				for(int i =0 ;i < white_board.size();i++)
				{
					if(white_board.get(i).type.equals("CREATE") || white_board.get(i).type.equals("DRAW"))
					{
						long cur = white_board.get(i).serverTime;
						int index = (int)(cur - start)/1000;

						if(white_board.get(i).userId.equals(tid))
						{
							points_time_map[index][0]+=white_board.get(i).dat.points.length;
						}
						else
						{
							points_time_map[index][1]+=white_board.get(i).dat.points.length;

						}
					}
				}
			}
		}
		return points_time_map;
	}
	public  ArrayList<Integer> Time_Map(HashMap<Integer,HashMap<String,ArrayList<WB>>> pages, String tid, String sid,long start, long end)
	{
		long range = end-start;
		ArrayList<Integer> time_map = new ArrayList<Integer>();

		for(int i =0;i< (int)((range/1000)+2);i++)
		{
			time_map.add(0);

		}
		for (Map.Entry<Integer, HashMap<String, ArrayList<WB>>> page_num_entry : pages.entrySet()) 
		{
			for (Map.Entry<String, ArrayList<WB>> userId : page_num_entry.getValue().entrySet())
			{
				ArrayList<WB> white_board = new ArrayList<WB>();
				white_board = userId.getValue();
				for(int i =0 ;i < white_board.size();i++)
				{
					if(white_board.get(i).type.equals("CREATE") || white_board.get(i).type.equals("DRAW"))
					{
						long cur = white_board.get(i).serverTime;
						int index = (int)(cur - start)/1000;

						if(white_board.get(i).userId.equals(tid))
						{
							time_map.set(index, 1);
						}
						else
						{
							time_map.set(index, 2);
						}
					}
				}
			}
		}
		return time_map;
	}
	public  ArrayList<String> page_analysis(HashMap<Integer,HashMap<String,ArrayList<WB>>> pages, String tid, String sid)
	{
		int image=0;
		int rect=0;
		int ellip =0;
		int trian = 0;
		int tcount=0;
		int scount=0;
		double wr_len=0.0;
		double w=1.0,h=1.0;
		String colour = "#000000";
		int change = 1;

		for (Map.Entry<Integer, HashMap<String, ArrayList<WB>>> page_num_entry : pages.entrySet()) 
		{
			Integer page_num = page_num_entry.getKey();

			int min_x,min_y,max_x,max_y;
			min_x = min_y = Integer.MAX_VALUE;
			max_x = max_y = Integer.MIN_VALUE;

			int len_x;
			int len_y;

			for (Map.Entry<String, ArrayList<WB>> userId : page_num_entry.getValue().entrySet()) 
			{
				String id  = userId.getKey();
				ArrayList<WB> white_board = new ArrayList<WB>();
				white_board = userId.getValue();

				for(int i =0 ;i < white_board.size(); i++)
				{
					try
					{
						if(white_board.get(i).dat.c == null)
						{
							continue;
						}
						else
						{
							if(!white_board.get(i).dat.c.equals(colour))
							{
								change++ ;
								colour = white_board.get(i).dat.c;
							}
						}
					}
					catch(Exception e)
					{
						
					}

					if(white_board.get(i).type.equals("CREATE"))
					{

						int size =white_board.get(i).dat.s;
						int num_points = white_board.get(i).dat.points.length;

						for(int m =0; m <num_points; m++)
						{
							if (min_x > white_board.get(i).dat.points[m][0])
								min_x = white_board.get(i).dat.points[m][0];

							if (min_y > white_board.get(i).dat.points[m][1])
								min_y = white_board.get(i).dat.points[m][1];

							if (max_x < white_board.get(i).dat.points[m][0])
								max_x = white_board.get(i).dat.points[m][0];

							if (max_y < white_board.get(i).dat.points[m][1])
								max_y = white_board.get(i).dat.points[m][1];
						}
						if((white_board.get(i).dat.points[num_points-1][0]!=0) || (white_board.get(i).dat.points[num_points-1][1]!=0))
						{
							if(white_board.get(i).userId.equals(tid))	// check with teacher userId
							{
								if(i < white_board.size()-1 && white_board.get(i).dat.id.equals(white_board.get(i+1).dat.id))
									tcount = tcount + (size*(white_board.get(i).dat.points.length-1));
								else tcount +=(white_board.get(i).dat.points.length*size);
							}
							else 
							{
								if(i < white_board.size()-1 && white_board.get(i).dat.id.equals(white_board.get(i+1).dat.id))
									scount +=(size*(white_board.get(i).dat.points.length-1));
								else scount +=(white_board.get(i).dat.points.length*size);
							}
						}
						if(white_board.get(i).dat.t.equals("DRAW"))
						{
							int len=white_board.get(i).dat.points.length;
							wr_len += ((size)*writeLength(white_board.get(i).dat.points[0][0],white_board.get(i).dat.points[len-1][0],white_board.get(i).dat.points[0][1],white_board.get(i).dat.points[len-1][1]));
						}

						if(white_board.get(i).dat.t.equals("ELLIPSE"))
						{
							int x =(white_board.get(i).dat.points[0][0]-white_board.get(i).dat.points[1][0]);
							int y =(white_board.get(i).dat.points[0][1]-white_board.get(i).dat.points[1][1]);
							ellip++;
							wr_len += Math.abs(x*y);
						}
						else if(white_board.get(i).dat.t.equals("RECT"))
						{
							int x = white_board.get(i).dat.points[0][0] - white_board.get(i).dat.points[1][0];
							int y = white_board.get(i).dat.points[0][1] - white_board.get(i).dat.points[1][1];
							rect++;
							wr_len += Math.abs(x*y);
						}
						else if(white_board.get(i).dat.t.equals("RIGHTTRIANGLE"))
						{
							int x = white_board.get(i).dat.points[0][0] - white_board.get(i).dat.points[1][0];
							int y = white_board.get(i).dat.points[0][1] - white_board.get(i).dat.points[1][1];
							trian++;
							wr_len += Math.abs(x*y);
						}
						else if(white_board.get(i).dat.t.equals("IMAGE"))
							image++;

					}

					else if(white_board.get(i).type.equals("UPDATE"))
					{
						image++;
					}
				}

			}
			//			w = pages.get(page_num).get(tid).get(1).dat.bw;
			//			h = pages.get(page_num).get(tid).get(1).dat.bh;
			w = 902;
			h=  600;
			len_x= max_x - min_x;
			len_y = max_y - min_y;
			
			int area_xy = len_x * len_y;
			double clumsiness = (area_xy)/(w*h);
			clum.add(clumsiness);

		}
		ArrayList<String> testcsv = new ArrayList();
		System.out.println("ellipses = " + ellip);
		testcsv.add(Integer.toString(ellip));
		System.out.println("rectangles = " + rect);
		testcsv.add(Integer.toString(rect));

		System.out.println("triangle =" + trian);
		testcsv.add(Integer.toString(trian));

		System.out.println("images ="+ image );
		testcsv.add(Integer.toString(image));

		System.out.println("colour change ="+ change);
		testcsv.add(Integer.toString(change));

		System.out.println("tcount = " + tcount);
		testcsv.add(Integer.toString(tcount));

		System.out.println("scount = " + scount);
		testcsv.add(Integer.toString(scount));

		System.out.println("TotalWriteLength =" + wr_len);
		testcsv.add(Double.toString(wr_len));

		double avg = 0.0;
		for(int index =0 ;index < clum.size();index++)
		{
			avg = avg + clum.get(index);
		}
		avg = avg*1.0/clum.size()*1.0;
		System.out.println("Average Clumsiness = " + avg);
		testcsv.add(Double.toString(avg));

		return testcsv;
	}
}
