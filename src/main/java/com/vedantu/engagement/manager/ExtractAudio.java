package com.vedantu.engagement.manager;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

class user
{
	String userId;
	String role;
	long billingPeriod;
	String joinTime;
	String userState;
}
class sessionInfo
{
	String sessionId;
	String subject;
	String startedAt;
	String endedAt;
	ArrayList<user> attendees;
}
class urlData
{
	String url;
}

class fileD
{
	long startTime;
}

class fileDetail
{
	String sessionId;
	String userId;
	String fileData;
	long stime;
	String fileId;
}
class fileMetaData
{
	ArrayList<fileDetail> data;
}
public class ExtractAudio
{
		public String[] ExtractFiles(String sessionId) throws UnsupportedEncodingException, IOException
		{
		//	String sessionId="6709647919546368";
			//-------------------------------------------
			File dir1 = new File("mp3");
			dir1.mkdir();	
			File dir = new File("mp3/"+ sessionId);
			dir.mkdir();
			int error =0;
			String[] str = new String[9];	//teacherid,student id,fileNos,subject,start time of audio
			
			String downloadurl = "https://vedantu-fos.appspot.com/_ah/api/sessionendpoint/v1/getsessioninfores?alt=json";	
			String payload = "{id: \""+sessionId+"\", callingUserId: 6051427639099392}";
			URL newobj = new URL(downloadurl);
			HttpURLConnection con = (HttpURLConnection) newobj.openConnection();
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			con.setDoInput(true);
			String USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.73 Safari/537.36";
			con.setRequestProperty("Accept", "application/json");
	        con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			con.setRequestProperty("User-Agent", USER_AGENT);
			
			//String line;
			//StringBuilder jsonString = new StringBuilder();
			
	        OutputStreamWriter finalwriter = new OutputStreamWriter(con.getOutputStream(), "UTF-8");
	        finalwriter.write(payload);
	        finalwriter.close();
	        BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
/*	        while ((line = br.readLine()) != null) {
	                jsonString.append(line);
	        }
	        br.close();
	*/    
	        
	        
	        
	/*        String session_info = jsonString.toString();
	        JsonParser new_parser = new JsonParser();
			JsonObject new_obj = new_parser.parse(session_info).getAsJsonObject();
			JsonArray attendees = new_obj.getAsJsonArray("attendees");
			if(attendees.get(0).getAsJsonObject().get("role").getAsString().equals("TEACHER"))
			{
				tid = attendees.get(0).getAsJsonObject().get("userId").getAsString();
				sid = attendees.get(1).getAsJsonObject().get("userId").getAsString();
			}
			else
			{
				tid = attendees.get(1).getAsJsonObject().get("userId").getAsString();
				sid = attendees.get(0).getAsJsonObject().get("userId").getAsString();
			}
		*/	
			
			//-------------------------------------------
			
			String rating=""+0;
			try
			{
				String downloadurlfeed = "https://vedantu-fos.appspot.com/_ah/api/feedbackendpoint/v1/fetchFeedback?alt=json";	
				String payloadfeed = "{sessionId: \""+sessionId+"\", orderDesc:true, callingUserId: 6051427639099392, userId: 6051427639099392}";
				URL newobjfeed = new URL(downloadurlfeed);
				HttpURLConnection confeed = (HttpURLConnection) newobjfeed.openConnection();
				confeed.setRequestMethod("POST");
				confeed.setDoOutput(true);
				confeed.setDoInput(true);
				String USER_AGENT_feed = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.73 Safari/537.36";
				confeed.setRequestProperty("Accept", "application/json");
				confeed.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
				confeed.setRequestProperty("User-Agent", USER_AGENT_feed);

				OutputStreamWriter finalwriterfeed = new OutputStreamWriter(confeed.getOutputStream(), "UTF-8");
				finalwriterfeed.write(payloadfeed);
				finalwriterfeed.close();
				BufferedReader brfeed = new BufferedReader(new InputStreamReader(confeed.getInputStream()));
				String line;
				StringBuilder jsonString = new StringBuilder();
				while ((line = brfeed.readLine()) != null) {
					jsonString.append(line);
				}
				brfeed.close();
				String feedback = jsonString.toString();
				JsonParser new_parser = new JsonParser();


				JsonObject new_obj = new_parser.parse(feedback).getAsJsonObject();
				JsonArray data = new_obj.getAsJsonArray("data");
				rating = data.get(1).getAsJsonObject().get("rating").getAsString();
			}
			catch(Exception e)
			{
				rating = ""+0;
			}
			str[5] = rating;
			System.out.println("rating = " +rating);
			//---------------------------------------------------------------------------------------------------------------------------						

	        
	        
			
		Gson gson = new Gson();


	//	BufferedReader br = new BufferedReader(	new FileReader("mp3\\Aarzoo Walia\\session_1\\session.json"));

		//convert the json string back to object
		sessionInfo obj = gson.fromJson(br, sessionInfo.class);
		String teacherId,studentId;
		long time_1;
		double late=0;
		System.out.println(sessionId+"  " +obj.attendees.get(0).userState +"  " +obj.attendees.get(1).userState);
		if(obj.attendees.get(0).userState.equals("NOT_JOINED") || obj.attendees.get(1).userState.equals("NOT_JOINED"))
		{
			str[0]= "-1";
			return str;
		}
		if(obj.attendees.get(0).role.equals("TEACHER"))
		{
			teacherId = obj.attendees.get(0).userId;
			time_1 = obj.attendees.get(0).billingPeriod;
			//if()
			late = Long.parseLong(obj.attendees.get(0).joinTime) - Long.parseLong(obj.startedAt);
			studentId = obj.attendees.get(1).userId;
		}
		else {
			teacherId = obj.attendees.get(1).userId;
			time_1 = obj.attendees.get(1).billingPeriod;
			late = Long.parseLong(obj.attendees.get(1).joinTime) - Long.parseLong(obj.startedAt);
			studentId = obj.attendees.get(0).userId;
		}
		str[0] = teacherId;
		str[1] = studentId;
		str[3] = obj.subject;
		late = 1.0*late/60000;
		if(late>0)
		str[8] = String.valueOf(late);
		else str[8] = "0";
		System.out.println("Lateness " + late);
		time_1 = time_1/1000;
		str[6] = Integer.toString((int)time_1);
		con.disconnect();
		br.close();
		String downloadurl1 = "https://vedantu-fos.appspot.com/_ah/api/filemgmtendpoint/v1/fetchFileMetadata?alt=json";	
		String payload1 = "{sessionId: \""+sessionId+"\", callingUserId: 6051427639099392}";
		URL newobj1 = new URL(downloadurl1);
		HttpURLConnection con1 = (HttpURLConnection) newobj1.openConnection();
		con1.setRequestMethod("POST");
		con1.setDoOutput(true);
		con1.setDoInput(true);
		String USER_AGENT1 = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.73 Safari/537.36";
		con1.setRequestProperty("Accept", "application/json");
        con1.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
		con1.setRequestProperty("User-Agent", USER_AGENT1);
		
		//String line;
		//StringBuilder jsonString = new StringBuilder();
		
        OutputStreamWriter finalwriter1 = new OutputStreamWriter(con1.getOutputStream(), "UTF-8");
        finalwriter1.write(payload1);
        finalwriter1.close();
        BufferedReader br1 = new BufferedReader(new InputStreamReader(con1.getInputStream()));
		
        fileMetaData fmd = gson.fromJson(br1, fileMetaData.class);
        con1.disconnect();
        br1.close();
        if(fmd.data != null){
        for(int i=0;i<fmd.data.size();i++)
        {
        	fileD time = gson.fromJson(fmd.data.get(i).fileData, fileD.class);
        	fmd.data.get(i).stime = time.startTime;
        	System.out.println(fmd.data.get(i).fileId);
        	System.out.print(fmd.data.get(i).userId + " ");
        	if(fmd.data.get(i).userId.equals(teacherId))
        		System.out.println("TEACHER");
        	else System.out.println("STUDENT");
        	System.out.println(fmd.data.get(i).sessionId);
        	System.out.println(fmd.data.get(i).stime);
        }
        }
        //--------------------------

        if(fmd.data!=null){
        Collections.sort(fmd.data, new Comparator<fileDetail>() {
            @Override public int compare(fileDetail p1, fileDetail p2) {
                return ((p1.stime < p2.stime)? -1: 1); // Ascending
            }
        });
        }
        
        //--------------------------
        
        
        int t=1;int s=1;
        if(error==0 && fmd.data != null){
        for(int i=0;i<fmd.data.size();i++)
        {
        	if(fmd.data.get(i).fileId != null)
        	{
        		
        	String downloadurl2 = "https://vedantu-fos.appspot.com/_ah/api/filemgmtendpoint/v1/fetchDownloadUrl?alt=json";	
    		String payload2 = "{id: \""+fmd.data.get(i).fileId+"\", callingUserId: 6051427639099392}";
    		URL newobj2 = new URL(downloadurl2);
    		HttpURLConnection con2 = (HttpURLConnection) newobj2.openConnection();
    		con2.setRequestMethod("POST");
    		con2.setDoOutput(true);
    		con2.setDoInput(true);
    		String USER_AGENT2 = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.73 Safari/537.36";
    		con2.setRequestProperty("Accept", "application/json");
            con2.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
    		con2.setRequestProperty("User-Agent", USER_AGENT2);
    		
    		//String line;
    		//StringBuilder jsonString = new StringBuilder();
    		
            OutputStreamWriter finalwriter2 = new OutputStreamWriter(con2.getOutputStream(), "UTF-8");
            finalwriter2.write(payload2);
            finalwriter2.close();
            BufferedReader br2 = new BufferedReader(new InputStreamReader(con2.getInputStream()));
    		urlData tempurl = gson.fromJson(br2, urlData.class);
           
            con2.disconnect();
            br2.close();
            
            
            URLConnection conn = new URL(tempurl.url).openConnection();
            InputStream is;
            try{
            is = conn.getInputStream();
            }
            catch(Exception e)
            {
            	System.out.println("Error while downloading "+fmd.data.get(i).fileId);
            	continue;
            }
            
            String name;
            if(fmd.data.get(i).userId.equals(teacherId))
            {
            	name = "mp3/"+ sessionId+"/t"+ (t++)+".mp3";
            	System.out.println(fmd.data.get(i).fileId + " with teacher id as " + fmd.data.get(i).userId + " and start time "+ fmd.data.get(i).stime + "is changed to " +name);
            }
            	else
            	{
            		name = "mp3/"+ sessionId+"/s"+ (s++)+".mp3";
            		System.out.println(fmd.data.get(i).fileId + " with student id as  " + fmd.data.get(i).userId + " and start time "+ fmd.data.get(i).stime + "is changed to " + name);
            	}
            		OutputStream outstream = new FileOutputStream(new File(name));
            byte[] buffer = new byte[4096];
            int len;
            while ((len = is.read(buffer)) > 0) {
                outstream.write(buffer, 0, len);
            }
            outstream.close();
        
        	}
        	else {
        		
        		error=1;
        		System.out.println("Not all files present. error " + error);
        		str[2] = Integer.toString(-1);
        		return str;
        		
        	}
        }
        }
        if(t!=s) {
        	//error=2;
        	System.out.println("teacher files is not equal to student files. error "+error);
        	
    		//return str;
        }
        str[7] = Integer.toString((s-1));
        if( t < 4 || s< 4 )
        {
        	
        	error =3;
        	System.out.println(" not enough files. error" + error);
        	str[2] = Integer.toString(-1);
    		return str;
        }
     	
		
		int k= t-1;
		if(error == 0)
		{
			str[2] =  Integer.toString(k);

		}
		else str[2] = Integer.toString(-1);
		
		str[4] = String.valueOf(fmd.data.get(0).stime);
		System.out.println(obj.subject);
		System.out.println(obj.startedAt);
		System.out.println(obj.attendees.get(0).userId + " " + obj.attendees.get(0).role);

		System.out.println(obj.attendees.get(1).userId + " "+ obj.attendees.get(1).role);
	//	return str;
		return str;
		}

}
