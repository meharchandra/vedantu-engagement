package com.vedantu.engagement.manager;
import java.util.*;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.swing.JComponent;

import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.util.JSON;
import com.vedantu.engagement.viewobject.response.GetRequestsResponse;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.math.*;
import java.net.UnknownHostException;
public class Engag 
{


	public ArrayList<String> minor_analysis(ArrayList<Double> tamp,ArrayList<Double> samp,String sessionId,String[] ids, String value_name,double pace) throws Exception
	{
		double Engagement_Index=0;
		double savg=0;
		int slen=0;
		double tavg=0;
		int tlen=0;
		int siz=0;
		if(samp.size()<tamp.size()) siz = samp.size();
		else siz = tamp.size();
		System.out.println(siz);
		int zflag=0;	int zcount=0;int zprev=0;
		double zero= (double)samp.get(0)- (double)tamp.get(0);
		ArrayList<ArrayList<String>> probs = new ArrayList<ArrayList<String>>();
		ArrayList<String> ind_prob = new ArrayList<String>();
		ind_prob.add(sessionId);
		ArrayList<ArrayList<String>> ques = new ArrayList<ArrayList<String>>();
		ArrayList<String> ind_ques = new ArrayList<String>();
		ind_ques.add(sessionId);
		double time_1 = 1.0*Integer.parseInt(ids[6])/(double)3600;
		if(time_1<=0)time_1=1;
		double tim = 1.0*(double)siz/3600.0;

		for(int i=0;i<siz;i++)
		{
			if((double)samp.get(i) > 500)
			{
				savg+=(double)samp.get(i);
				slen++;
			}	
			if((double)tamp.get(i) > 500)
			{
				tavg+=(double)tamp.get(i);
				tlen++;
			}
			if(i>0)
			{
				zero= (double)samp.get(i)- (double)tamp.get(i);
				if(zero < 0 && zflag ==0 && (samp.get(i)>400 || tamp.get(i)>400))
				{
					if((i-zprev) >2)
					{
						zcount++;
						zprev =i;
					}
					//zcount++;
					zflag=1;
				}
				else if (zero > 0 && zflag ==1 && (samp.get(i)>400 || tamp.get(i)>400))
				{
					if((i-zprev) >2)
					{
						zcount++;
						zprev =i;
					}
					zflag=0;
				}
			}


		}
		savg=savg/slen;
		tavg=tavg/tlen;

		double interrupt=0;
		int prob_solving_tolerance=3;  
		double pause=0;
		int tquesflag=0;
		int probflag=0;
		int student_solving_time=0;
		int slow_teacher=0;
		double teacher_time=0;
		double student_time=0;
		double acknowledge=0; 
		int t_rhet=0;
		double student_answering_time=0;
		int tquestions=0;
		int problemgiven=0;
		int echo=0;
		int disturbance=0;
		int teacher_whiteboard_usage=0;
		int student_whiteboard_usage=0;
		int student_paper=0;
		int nothing=0;
		int sametime=0;
		ArrayList<String> export= new ArrayList<String>();
		//int icount=0;
		//	System.out.println("Continue0");
		mehar_redesign wb_analysis = new mehar_redesign();
		//wb_analysis.check();
		//	System.out.println("Continue2");
		//	export.add(sessionId);
		ArrayList<String> tempA = wb_analysis.check(sessionId,ids);
		//	List<String> temp_edited = tempA.subList(17,26);
		//	String hwr = tempA.get(17);
		//	tempA.remove(17);
		tempA.remove(27);
		ArrayList<String> temp_edited = new ArrayList<String>();
		temp_edited.add(sessionId);
		temp_edited.add(ids[3]);
		for(int t=17;t<=28;t++)
		{
			if(t!=21 && t !=24 && t!=26 && t!=27 && t!=28)
				temp_edited.add(String.valueOf((Double.parseDouble(tempA.get(17))/time_1)));
			else temp_edited.add(tempA.get(17));
			//		System.out.println(temp_edited.get(t-15));
			tempA.remove(17);
		}
		System.out.println("Check "+ temp_edited.get(13));
		int mm;
		if(tempA.get(5).equals("0"))
			mm=1;
		else mm=0;
		temp_edited.add(3,String.valueOf(mm));
		tempA.remove(0);
		int delta = Integer.parseInt(tempA.get(0));
		tempA.remove(0);
		//	if(delta<0)delta=0;
		export.addAll(tempA);
		//	System.out.println("Continue4");
		//	export.add(hwr);
		for(int i=0;i<siz;i++)
		{
			if((double)tamp.get(i)>1500 && (double)samp.get(i)>1500)
			{
				interrupt+=1;
				//icount++;
				//if(icount >3) ;
				// subtract from interrupt
				//Problem in connection
			}
			// else icount =0;
			if((double)tamp.get(i)>500 && ( (double)tamp.get(i)-(double)samp.get(i))>300)  // Change Made from hard coded value to one based on difference
			{
				teacher_time+=1;
				if(i+delta<wb_analysis.points_time_map.length && i+delta >0)
				{
					if(wb_analysis.points_time_map[i+delta][0]>0)
					{
						sametime++;
					}

				}
			}
			else if((double)samp.get(i)>500 && ( (double)samp.get(i)-(double)tamp.get(i))>300)
				student_time+=1;
			else if((double)samp.get(i)<200 && (double)tamp.get(i)<200)
				pause+=1;
			else if((double)tamp.get(i)>20 && (double)tamp.get(i)<50)
				echo+=1;
			else if((double)tamp.get(i)<100 && (double)tamp.get(i)>50)
				disturbance++;
			else {
				if(i+delta<wb_analysis.points_time_map.length && i+delta >0)
				{
					if(wb_analysis.points_time_map[i+delta][0]<20)
					{
						nothing++;
					}
				}
			}
		}
		System.out.println("echo " + echo + "\n disturbance " + disturbance);
		int temp;
		int i=1;
		int counter=1;
		int answer_1=0;
		int teach_wb=0;int stud_wb=0;
		int y0flag=0;
		//int tquesflag=0;
		int total_stime=0;

		while(i<siz-4)
		{
			if(((double)tamp.get(i-1) > (double)tamp.get(i)) && ((double)tamp.get(i) > (double)tamp.get(i+1)) && i>1)
			{
				int store = i;

				if(((((double)samp.get(store)>500) || (double)samp.get(store+1)>500)) && ((double)samp.get(store+2)- (double)tamp.get(store+2) > 300))    // store +2 is made relative difference instead of absolute values
				{
					// System.out.println("Proper Question");
					store++;
					i++;
					int lala;
					while((((double)samp.get(store) - (double)tamp.get(store))>300) && store < siz-1)  // Changes here for getting dominant voice
					{

						tquesflag=1;
						store++;
						i++;
						student_answering_time++;
						answer_1++;
					}
					/*		if((((double)samp.get(store+1) - (double)tamp.get(store+1))>300) || (((double)samp.get(store+2) - (double)tamp.get(store+2))>300))
					{
						for()
					}
					 */		
					if(tquesflag == 1)
					{

						if(answer_1> 4)
						{
							tquestions++;
							System.out.println("answer time " + answer_1 + " at timestamp " + i);
							ind_ques.add(Integer.toString((i-answer_1)));
							ind_ques.add(Integer.toString(answer_1));
						}
						answer_1=0;

					}
					tquesflag=0;

				}

				else if (((double)samp.get(store)> 300 && (double)samp.get(store+1)<500) || ((double)samp.get(store+1)>300 && (double)samp.get(store+2)<500))
				{
					t_rhet+=1;
					i++;
					acknowledge++;
					// rhetorical
				}

				else if (((double)samp.get(store)<100 && (double)samp.get(store+1)<100) && (double)tamp.get(store)< 300 )
				{
					int klm = store;
					while(counter < prob_solving_tolerance)
					{
						//	System.out.println("Problem---- ");
						if((double)tamp.get(store+counter)<400 && (double)samp.get(store+counter)<400 && store < (samp.size()-1))
						{
							counter++;
						}
						else 
						{
							y0flag=1;
							break;
						}

					}
					counter =1;
					if(y0flag==0)
					{
						temp=store;
						int tmpflag=0;
						int addTime=0;
						int prob_ttime=0;
						while(tmpflag==0)
						{

							if(store < tamp.size()-2)
							{
								while((double)tamp.get(store)<200  && (double)samp.get(store)<600 && store < siz-2 )
								{
									student_solving_time++;
									store++;
									i++;
								}
							}
							tmpflag=1;
							store++;i++;
							for(int tmp=0;tmp<3;tmp++)
							{
								int tt = store+tmp;
								if(tt< (siz-2))
								{
									if((double)tamp.get(tt)<200  && (double)samp.get(tt)<400 && tt < siz-2)
									{
										store +=tmp;
										if(store<tamp.size()-2)
										{
											tmpflag=0;
											//	break;
										}
										//	else break;
									}
								}
							}
						}

						store++;//i++;
						student_solving_time+=addTime;
						if(student_solving_time>2)
						{
							teach_wb=0;
							stud_wb=0;

							//	System.out.println(temp);
							for(int tm =temp;tm < store; tm++)
							{
								if(tm+delta < wb_analysis.points_time_map.length && tm+delta>0)
								{
									teach_wb+= wb_analysis.points_time_map[tm+delta][0];
									stud_wb+= wb_analysis.points_time_map[tm+delta][1];
								}
							}
							for(int prob_wb=temp-3;prob_wb< temp+3;prob_wb++)
							{
								if(prob_wb+delta < wb_analysis.points_time_map.length && prob_wb+delta>0)
								{
									prob_ttime+= wb_analysis.points_time_map[prob_wb+delta][0];

								}
								////IMages check for problem

								//imo
							}
							int img_check=0;
							for(int img=0;img<wb_analysis.image_time.size();img++)
							{
								if((wb_analysis.image_time.get(img)/1000 + delta) > temp-5 || (wb_analysis.image_time.get(img)/1000 + delta) > temp+5)
								{
									img_check=1;
									break;
								}
							}
							if(teach_wb ==0)
								teacher_whiteboard_usage-=2;
							else teacher_whiteboard_usage+=teach_wb;

							if(stud_wb == 0)
								student_paper++;
							else student_whiteboard_usage = 2*stud_wb;
							/*IMPORTANT
							 * */
							if(student_solving_time> 10 && (prob_ttime>20 || img_check==1))	//				 
							{ problemgiven++;
							System.out.println(student_solving_time+" solving_test " + temp);
							ind_prob.add(Double.toString((double)klm*1.0/60));
							ind_prob.add(Integer.toString(student_solving_time));

							}
						}
						total_stime+= student_solving_time;
						student_solving_time=0;
					}

					else
					{
						i++;
						y0flag=0;				
					}					
				}
				else i++;							
			}
			else i++;
		}

		int session_length= tamp.size();
		System.out.println(session_length);
		acknowledge = (acknowledge/session_length)*100;
		double newInterrupt = (interrupt/session_length)*100;
		///////////////////////////////////////////////////////
		teacher_time= (teacher_time/session_length)*100;
		student_time= (student_time/session_length)*100;
		double newPause= (pause/session_length)*100;

		int pause_weight= -1;
		int interrupt_weight = 1;
		int tques_weight = 4;
		int ack_weight = 4;
		int interaction_weight=0;
		double t_wb=teacher_whiteboard_usage;
		double s_wb=student_whiteboard_usage;
		double s_paper=4*student_paper;
		double avg_problem_duration=1;
		double problem_weight= ((double)(1.0*t_wb+s_wb+s_paper)/session_length)*100;
		problem_weight = 2;
		//problem_weight = 
		if(problemgiven>0)
		{
			avg_problem_duration= total_stime/problemgiven;
		}

		if(((double)student_time/teacher_time)<0.3)
			interaction_weight=-6;
		else interaction_weight=6;

		System.out.println("session length " + session_length);
		export.add(String.valueOf(ids[6]));

		int ttemp =zcount/3;
		//	System.out.println("Check 2 "+ temp_edited.get(14));
		temp_edited.add(String.valueOf(time_1));

		if(tim >= 0.8* time_1 && teacher_time>10)
		{		
			//	
			temp_edited.add(String.valueOf(1.0*(double)(interrupt+tquestions+(t_rhet/2)+ttemp)/tim));
			//

			temp_edited.add(String.valueOf((double)(problemgiven)/tim));
			temp_edited.add(String.valueOf(teacher_time));
			//System.out.println(/*"student_time = " +*/ student_time);
			temp_edited.add(String.valueOf(student_time));
			temp_edited.add(String.valueOf((double)1.0*student_time/teacher_time));
			temp_edited.add(String.valueOf((double)nothing/tim));
			temp_edited.add(String.valueOf((double)sametime/tim));
			temp_edited.add(String.valueOf(pace));
			temp_edited.add(String.valueOf(1.0*(double)session_length/3600.0));

			Scoring_1 scr = new Scoring_1(temp_edited);
			temp_edited.add(String.valueOf(scr.ans[0]));

			if(scr.ans[0]<=8 || scr.ans[1]>4)
				temp_edited.add("Bad");
			else temp_edited.add("Not Bad " + scr.ans[1]);

		//	Database database = new Database(temp_edited);
		}

		//	System.out.println("Interaction Weight = " + interaction_weight);
		System.out.println(/*"interrupt = " +*/ interrupt);
		export.add(String.valueOf(interrupt));
		System.out.println(/*"tquestions = " +*/ tquestions);
		export.add(String.valueOf(tquestions));
		System.out.println(/*"trhet = " +*/ t_rhet/2);
		export.add(String.valueOf(t_rhet/2));
		System.out.println(/*"pause = " +*/ pause);
		export.add(String.valueOf(pause));
		System.out.println(/*"teacher_time = " +*/ teacher_time);
		export.add(String.valueOf(teacher_time));
		System.out.println(/*"student_time = " +*/ student_time);
		export.add(String.valueOf(student_time));
		System.out.println(/*"stime = " +*/ total_stime);
		export.add(String.valueOf(total_stime));
		System.out.println(/*"nothing = " +*/ nothing);
		export.add(String.valueOf(nothing));
		System.out.println(/*"problem given = " +*/ problemgiven);
		export.add(String.valueOf(problemgiven));
		System.out.println(/*"crossings = " +*/ zcount/3);

		export.add(String.valueOf(ttemp));
		System.out.println(/*"sametime = " +*/ sametime);
		export.add((String.valueOf(sametime)));

		//	problemgiven=0;
		double pause_index= ((double)(pause-total_stime))/session_length;
		pause_index = 100*pause_index*pause_weight;
		double interrupt_index = ((double)interrupt)/session_length;
		interrupt_index = 100*interrupt_index*interrupt_weight;
		double tques_index = ((double)tquestions*3)/session_length;
		tques_index = 100*tques_index*tques_weight;
		double trhet_index = ((double)(t_rhet*ack_weight))/session_length;
		trhet_index = trhet_index*100;
		/*		
		System.out.println("pause index = " + pause_index);
		System.out.println("interrupt_index = " + interrupt_index);
		System.out.println("tques_index = " + tques_index);
		System.out.println("trhet_index = " + trhet_index);
		 */	
		probs.add(ind_prob);
		ques.add(ind_ques);
		CsvWriter csv = new CsvWriter();
		csv.report(probs,"air_probs.csv");
		CsvWriter csv2 = new CsvWriter();
		csv2.report(ques,"air_ques.csv");
		ArrayList<ArrayList<String>> edited_out = new ArrayList<ArrayList<String>>();
		edited_out.add(temp_edited);
		CsvWriter csv1 = new CsvWriter();
		csv1.report(edited_out,"edited_doc.csv");
		Engagement_Index=interaction_weight + pause_index + interrupt_index + 
				tques_index + trhet_index +
				(problem_weight*100*(problemgiven*avg_problem_duration/2)/session_length);
		//		System.out.println(Engagement_Index);
		if(newPause > 45)
			Engagement_Index -=10;
		if(teacher_time <20)
			Engagement_Index-=10;		
		export.add(String.valueOf(Engagement_Index));
		export.add(String.valueOf(pace));
		FileWriter outFile = new FileWriter(value_name);
		BufferedWriter outStream = new BufferedWriter(outFile);
		FileWriter outFile1 = new FileWriter("weird_id.txt",true);
		BufferedWriter outStream1 = new BufferedWriter(outFile1);
		try {
			outStream.write("session length = " + session_length + "\n");
			outStream.write("Interaction Weight = " + interaction_weight + "\n");
			outStream.write("interrupt = " + interrupt + "\n");
			outStream.write("tquestions = " + tquestions + "\n");
			outStream.write("trhet = " + t_rhet + "\n");
			outStream.write("pause = " + pause + "\n");
			outStream.write("teacher_time = " + teacher_time + "\n");
			outStream.write("student_time = " + student_time + "\n");
			outStream.write("stime = " + total_stime  + "\n");
			outStream.write("nothing = " + nothing  + "\n");
			outStream.write("pause_index = " + pause_index  + "\n");
			outStream.write("interrupt_index = " +interrupt_index+ "\n");
			outStream.write("tques_index = " + tques_index  + "\n");
			outStream.write("trhet_index = " + trhet_index + "\n");
			outStream.write("Engagement_Index = " + Engagement_Index);

			if(teacher_time<10)outStream1.write("sessionId\n");

		} catch (IOException e) {

			e.printStackTrace();
		}
		outStream.close();

		outStream1.close();

		return temp_edited;

	}

	public  void fileprint(String filename,ArrayList amp)
	{
		try {
			FileWriter outFile = new FileWriter(filename);
			BufferedWriter outStream = new BufferedWriter(outFile);
			for (int k = 0; k < amp.size(); k++)
				outStream.write(amp.get(k).toString() + "\n");
			outStream.close();
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}


	public  ArrayList readFile(String filename)
	{
		ArrayList<String> k = new ArrayList<String>();
		try (BufferedReader br = new BufferedReader(new FileReader(filename)))
		{

			String sCurrentLine;

			while ((sCurrentLine = br.readLine()) != null) {
				k.add(sCurrentLine);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} 
		return k;

	}


	public  ArrayList<Double> stitch(ArrayList<Double> alist1)
	{
		int i,fs=8000;
		int avg = 0;
		double y0;
		ArrayList<Double> tamps = new ArrayList<Double>();
		for(i=0;i<alist1.size();i++)
		{
			if(i%fs == 0)
			{
				y0= avg/fs;
				tamps.add(y0);
				avg=0;
			}
			avg += Math.abs((double)alist1.get(i));
		}

		return (tamps);
	}

	public  ArrayList<String> count(ArrayList<Double> teach,ArrayList<Double> stud,String[] ids, String value_name,String amp_png, String sessionId) throws Exception
	{
		int i,fs=8000;
		int avg = 0;
		double y0;
		ArrayList<Double> tamps = new ArrayList<Double>();
		ArrayList<Double> samps = new ArrayList<Double>();
		tamps = stitch(teach);
		samps = stitch(stud);
		//		Plot_AL chart = new Plot_AL("Amp over time", "amp", tamps,samps,value_name);
		//		chart.create(tamps);

		/*	int[] c = new int[10];
		int[] s = new int[10];
		int l,m;
		for(i=0;i<10;i++)
		{
			c[i]=0;
			s[i]=0;
		}
		for(i=0;i<tamps.size();i++)
		{
			if((double)tamps.get(i)<900)
			{
				l = (int)(double)tamps.get(i);
				c[l/100]+=1;
			}
			else c[9]+=1;
			if(i < samps.size())
			{
				if((double)samps.get(i)<900)
				{
					m = (int)(double)samps.get(i);
					s[m/100]+=1;
				}
				else s[9]+=1;

			}
		}
		for(i=0;i<10;i++)
		{
			System.out.println(i+" " + c[i]);
		}
		System.out.println();
		for(i=0;i<10;i++)
		{
			System.out.println(i+" " + s[i]);
		}
		 */
		//	fileprint("dataa/text_data/teacher_5.txt",tamps);
		//	fileprint("dataa/text_data/student_5.txt",samps);




		ArrayList<String> Eng= minor_analysis(tamps,samps,sessionId,ids,value_name,0.0);
		return Eng;
		//return 0;
	}

	public  ArrayList<String> WBvalue(String sessionId, String[] st) throws IOException, Exception
	{
		System.out.println(Integer.parseInt(st[2]));
		mehar_redesign wb_analysis = new mehar_redesign();
		double tim = 1.0*(double)Integer.parseInt(st[6]);
		tim = tim/3600;
		ArrayList<String> ind = wb_analysis.check(sessionId, st);
		if(ind.size()<26)return null;
		//		String hwr = ind.get(17);
		//	ind.remove(17);
		ind.remove(27);
		ArrayList<String> tmp_e = new ArrayList<String>();
		tmp_e.add(sessionId);
		tmp_e.add(st[3]);
		for(int t=17;t<=28;t++)
		{

			if(t!=21 && t !=24 && t!=26 && t!=27 && t!=28)
				tmp_e.add(String.valueOf((Double.parseDouble(ind.get(17))/tim)));
			else tmp_e.add(ind.get(17));
			//			System.out.println(tmp_e.get(t-15));
			ind.remove(17);



		}
		int mm;
		if(ind.get(5).equals("0"))
			mm=1;
		else mm=0;
		tmp_e.add(3,String.valueOf(mm));
		tmp_e.add(String.valueOf(tim));
		String lm = "";
		if(ind.size() >3)
			lm = ind.get(0);
		ind.remove(0);
		ind.remove(0);
		ind.add(0,sessionId);
		ind.add(1, st[5]);
		ind.add(2, st[3]);
		ind.add(3, st[0]);

		ind.add(4, st[1]);
		//	ind.add(lm);
		ind.add(5,st[8]);
		//	ind.add(hwr);
		ind.add(st[6]);

		ArrayList<ArrayList<String>> temp_out = new ArrayList<ArrayList<String>>();
		ArrayList<ArrayList<String>> temp_e = new ArrayList<ArrayList<String>>();
		temp_out.add(ind);
		temp_e.add(tmp_e);
		CsvWriter csv1 = new CsvWriter();
		csv1.report(temp_e,"edited_doc.csv");
		CsvWriter csv = new CsvWriter();
		csv.report(temp_out,"air_new1.csv");
		//	System.out.println("big difference in audio length ");
		return tmp_e;
	}


	public GetRequestsResponse getEngage(Long id) throws Exception 
	{

		
		String finname1,finname2,foutname1,foutname2;
		double index;
		finname1= "dataa/teacher_2.mp3";
		finname2="dataa/student_2.mp3";

		ArrayList<ArrayList<String>> finalout = new ArrayList<ArrayList<String>>();
		//ArrayList<String> sids = readFile("sessionidslist.txt");	//temp_id, test_id done
		ArrayList<String> sids = new ArrayList<String>();
		sids.add(Long.toString(id));
		ArrayList<String> index_1 =new ArrayList<String>();
		for(int sessionnos=0;sessionnos<sids.size();sessionnos++)
		{
			ArrayList<Double> teacher=new ArrayList<Double>();
			ArrayList<Double> student=new ArrayList<Double>();
			String sessionId = sids.get(sessionnos);
			//String[] st = 
			ExtractAudio ea = new ExtractAudio();
			String[] st = ea.ExtractFiles(sessionId);
			if(st[0].equals("-1"))continue;
			System.out.println("hello "+ st[2]);
			int abc=0;
			abc = Integer.parseInt(st[2]);

			if(abc>0)
			{
				double []tcher = new double[abc];
				double tcher1=0;
				int div=0;
				for(int i =0;i<abc;i++)
				{
					foutname1="out1.wav";

					MP3toWav mp3t=new MP3toWav(("mp3/"+sessionId+"/t"+(i+1) +".mp3"),foutname1);
					Wav2Text wavt= new Wav2Text(foutname1);
					wavt.convert();
					teacher.addAll(stitch(wavt.x));
					Pace pc = new Pace();
					tcher[i]= pc.Pace(wavt.x);
					if(tcher[i]!=0){

						tcher1 += tcher[i];
						div+=1;
					}
					System.out.println("teacher size "+ teacher.size());

				}
				for(int i =0;i<Integer.parseInt(st[7]);i++)
				{
					foutname2="out2.wav";
					MP3toWav mp3s=new MP3toWav(("mp3/"+sessionId+"/s"+(i+1) +".mp3"),foutname2);
					Wav2Text wavs= new Wav2Text(foutname2);
					wavs.convert();
					student.addAll(stitch(wavs.x));
					System.out.println("student size "+ student.size());

				}
				int an_flag;
				//if(abc != Integer.parseInt(st[7]))
				//{
				int diff=teacher.size() - student.size();	
				if(diff<=200 && diff>=-200)
					an_flag = 1;
				else an_flag =0;
				//	}
				if(teacher.size() < .5*student.size() || student.size()<.5*teacher.size())
				{
					FileWriter outFile = new FileWriter("weird_id.txt",true);
					BufferedWriter outStream = new BufferedWriter(outFile);
					outStream.write(sessionId + " t " +teacher.size() +" s "+student.size()+ "\n");
				}

				if(div>0)
					tcher1 = tcher1/div;
				index_1 =new ArrayList<String>();
				if(an_flag ==1)
				{index_1= minor_analysis(teacher,student,sessionId,st,"teacher_1.txt",tcher1);
				//	System.out.println("Continue_new");

			/*	index_1.add(0,sessionId);
				index_1.add(1,st[5]); //rating
				index_1.add(2, st[3]);
				index_1.add(3, st[0]);
				index_1.add(4, st[1]);
				index_1.add(5,st[8]);
*/
				//	Plot_AL chart = new Plot_AL("Amp over time", "amp", teacher,student,"name_1234");
				//	chart.create(teacher);
				//	if(tcher1 <120 )tcher1 = (tcher1/10)+120;
				System.out.println(tcher1);
				//	index_1.add(String.valueOf(tcher1));
				}
				else index_1 = WBvalue(sessionId, st);
				finalout.add(index_1);
				ArrayList<ArrayList<String>> temp_out = new ArrayList<ArrayList<String>>();
				temp_out.add(index_1);
				CsvWriter csv = new CsvWriter();
				csv.report(temp_out,"air_new1.csv");
			}
			else 
			{
				index_1 = WBvalue(sessionId,st);
				if(index_1 != null)
					finalout.add(index_1);

			}
		}
		GetRequestsResponse getRequestsResponse = new GetRequestsResponse(index_1);
		CsvWriter csv = new CsvWriter();
		Collections.sort(finalout, new Comparator<ArrayList<String>>() {
			@Override
			public int compare(ArrayList<String> o1, ArrayList<String> o2) {
				return o2.get(1).compareTo(o1.get(1));
			}   
		});
		csv.report(finalout,"air_new_temmp.csv");
		//		}System.out.println("index " + index_1);
		//--------------------IMP----------------------------
		//		Plot_AL chart = new Plot_AL("Amp over time", "amp", teacher,student,"name_1234");
		//		chart.create(teacher);
		return getRequestsResponse;

	}



}
