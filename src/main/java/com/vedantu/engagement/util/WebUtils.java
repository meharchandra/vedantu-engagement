package com.vedantu.engagement.util;

import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

@Service
public class WebUtils {

	private Client client;
	public static final WebUtils INSTANCE = new WebUtils();

	public WebUtils() {
		client = Client.create();
		client.setConnectTimeout(1000);
		client.setReadTimeout(10000);
	}

	public ClientResponse doCall(String serverUrl, HttpMethod requestType, String query) {

		WebResource webResource = client.resource(serverUrl);

		ClientResponse response = null;
		switch (requestType) {
		case POST:
			response = webResource.type("application/json").post(ClientResponse.class, query);
			break;
		case PUT:
			response = webResource.type("application/json").put(ClientResponse.class, query);
			break;
		case GET:
			response = webResource.type("application/json").get(ClientResponse.class);
			break;
		case DELETE:
			response = webResource.type("application/json").delete(ClientResponse.class, query);
			break;
		default:
			return null;
		}

		return response;
	}
	
	public ClientResponse doCall(String serverUrl, HttpMethod requestType, String query, String authHeader) {

		WebResource webResource = client.resource(serverUrl);
		ClientResponse response = null;
		switch (requestType) {
		case POST:
			response = webResource.type("application/json").header("Authorization", authHeader).post(ClientResponse.class, query);
			break;
		case PUT:
			response = webResource.type("application/json").header("Authorization", authHeader).put(ClientResponse.class, query);
			break;
		case GET:
			response = webResource.type("application/json").header("Authorization", authHeader).get(ClientResponse.class);
			break;
		case DELETE:
			response = webResource.type("application/json").header("Authorization", authHeader).delete(ClientResponse.class, query);
			break;
		default:
			return null;
		}

		return response;
	}

}
