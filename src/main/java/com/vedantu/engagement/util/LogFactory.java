package com.vedantu.engagement.util;

import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class LogFactory {

	private ConfigUtils configUtils = new ConfigUtils();
	
	private static Logger logger = LogManager.getRootLogger();

	public LogFactory() {
		logger.info("log factory initialized");
		logger.info(configUtils);
		String log4jConfigFile = "/ENV-"
				+ configUtils.properties.getProperty("environment")
				+ File.separator + "log.xml";
		System.setProperty("log4j.configurationFile", log4jConfigFile);
		logger.info(log4jConfigFile);
		logger.error("Test Error Message");
		logger.info("Vedantu Platform Initialized");
		logger.info("Logging Enabled");
	}

	public static Logger getLogger(Class<?> clazz) {

		return logger = LogManager.getLogger(clazz);
	}

}
