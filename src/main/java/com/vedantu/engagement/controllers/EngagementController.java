package com.vedantu.engagement.controllers;

import java.util.ArrayList;
import java.util.List;
import java.lang.Exception;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.engagement.manager.EngagementManager;
import com.vedantu.engagement.util.LogFactory;
import com.vedantu.engagement.viewobject.response.*;

@RestController
@RequestMapping("engagement")
public class EngagementController {

	@Autowired
	private EngagementManager engagementManager;
	
	@Autowired
	private LogFactory logfactory;
	private Logger logger = logfactory.getLogger(EngagementController.class);
	
	@RequestMapping(value = { "/getSessionEngagement/{id}" }, method = RequestMethod.GET)
	@ResponseBody
	public GetRequestsResponse getEngagementRequest(@PathVariable("id") Long id) throws Exception {

		logger.info("Request received for getting Engagement for sessionId: " + id);
		GetRequestsResponse getRequestsResponse = engagementManager.getEngagement(id);
		logger.info("Response:" + getRequestsResponse.toString());
		return getRequestsResponse;
	}
}
