package com.vedantu.engagement.dao.serializers;

	import java.util.Collection;
	import java.util.List;

	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.data.mongodb.core.query.Criteria;
	import org.springframework.data.mongodb.core.query.Query;
	import org.springframework.data.mongodb.core.query.Update;
	import org.springframework.stereotype.Service;
	import org.springframework.util.StringUtils;

	import com.mongodb.WriteResult;
import com.vedantu.engagement.viewobject.response.GetRequestsResponse;

	@Service
	public class AbstractEngagementDAO {

		public static final String ORDER_DESC = "desc";
		public static final String ORDER_ASC = "asc";
		public static final String QUERY_AND_OPERATOR = "&&";

		public static final long NO_START = 0;
		public static final long NO_LIMIT = -1;
		public static final long UNINITIALIZED = -1L;

		@Autowired
		private static AbstractMongoConfiguration abstractMongoConfiguration = new AbstractMongoConfiguration();

		public AbstractEngagementDAO() {
			super();
		}

		protected static <E extends GetRequestsResponse> void saveEntity(E entity) {
			
			abstractMongoConfiguration.getMongoOperations().save(entity, entity.getClass().getSimpleName());
		}

		protected static <E extends GetRequestsResponse> void saveAllEntities(Collection<E> entities, String collectionName) {
			//setEntityDefaultProperties(entities);
			abstractMongoConfiguration.getMongoOperations().insert(entities, collectionName);
		}

		protected static <E extends GetRequestsResponse> void updateEntity(E entity, Query q, Update u) {
		//	setEntityDefaultProperties(entity);
			abstractMongoConfiguration.getMongoOperations().upsert(q, u, entity.getClass().getSimpleName());
		}

		public <T extends GetRequestsResponse> List<T> getEntities(Class<T> calzz) {
			return abstractMongoConfiguration.getMongoOperations().findAll(calzz, calzz.getSimpleName());
		}

		public <T extends GetRequestsResponse> T getEntityById(String id, Class<T> calzz) {
			Query query = new Query(Criteria.where("_id").is(id));
			return abstractMongoConfiguration.getMongoOperations().findOne(query, calzz, calzz.getSimpleName());
		}

		public <T extends GetRequestsResponse> List<T> runQuery(Query query, Class<T> calzz) {
			return abstractMongoConfiguration.getMongoOperations().find(query, calzz, calzz.getSimpleName());
		}

		public <T extends GetRequestsResponse> int queryCount(Query query, Class<T> calzz) {
			return abstractMongoConfiguration.getMongoOperations().find(query, calzz, calzz.getSimpleName()).size();
		}

		public <T> int deleteEntityById(String id, Class<T> calzz) {
			Query query = new Query(Criteria.where("_id").is(id));
			WriteResult result = abstractMongoConfiguration.getMongoOperations().remove(query, calzz,
					calzz.getSimpleName());
			return result.getN();
		}


	}



