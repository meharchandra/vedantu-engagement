package com.vedantu.engagement.dao.serializers;

	import java.util.List;

	import org.springframework.data.mongodb.core.query.Query;
	import org.springframework.data.mongodb.core.query.Update;
	import org.springframework.stereotype.Service;
	import org.springframework.util.StringUtils;

import com.vedantu.engagement.viewobject.response.GetRequestsResponse;

	@Service
	public class EngagementDAO extends AbstractEngagementDAO {

		public EngagementDAO() {
			super();
		}

		public void create(GetRequestsResponse p) {
			try {
				if (p != null) {
					saveEntity(p);
				}
			} catch (Exception ex) {
			}
		}

		public GetRequestsResponse getById(String id) {
			GetRequestsResponse GetRequestsResponse = null;
			try {
				if (!StringUtils.isEmpty(id)) {
					GetRequestsResponse = (GetRequestsResponse) getEntityById(id, GetRequestsResponse.class);
				}
			} catch (Exception ex) {
				GetRequestsResponse = null;
			}
			return GetRequestsResponse;
		}

		public void update(GetRequestsResponse p, Query q, Update u) {
			try {
				if (p != null) {
					updateEntity(p, q, u);
				}
			} catch (Exception ex) {
			}
		}

		public int deleteById(String id) {
			int result = 0;
			try {
				result = deleteEntityById(id, GetRequestsResponse.class);
			} catch (Exception ex) {
			}

			return result;
		}

		public List<GetRequestsResponse> getAll() {
			List<GetRequestsResponse> result = getEntities(GetRequestsResponse.class);
			return result;
		}
	}
