package com.vedantu.engagement.dao.serializers;

import java.net.UnknownHostException;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.vedantu.engagement.util.LogFactory;

public class MongoClientFactory {

	@Autowired
	private LogFactory logFactory;
	
	private Logger logger = logFactory.getLogger(MongoClientFactory.class);
	
	private static final String HOST = "localhost";
	private static final int PORT = 27017;
	private MongoClient client = null;
	
	public final static MongoClientFactory INSTANCE = new MongoClientFactory();
	
	public MongoClientFactory(){
		logger.info("MongoClientFactory is inialized");
		MongoClientOptions.Builder builder = new MongoClientOptions.Builder();
		MongoClientOptions options = builder.connectionsPerHost(10).build();
		String serverUrl = HOST + ":" + PORT;
		try {
			client = new MongoClient(serverUrl, options);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	public MongoClient getClient() {
		logger.info("MongoClientFactory get client called");
		return client;
	}

	public void setClient(MongoClient client) {
		this.client = client;
	} 
	
}
