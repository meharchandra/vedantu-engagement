package com.vedantu.engagement.viewobject.response;

import java.util.ArrayList;
import java.util.List;

public class GetRequestsResponse {

	private String _id;
	private String Subject;
	private String ShapesandImages;
	private String NoImages;
	private String ColourChanges;
	private String TeacherWriteLength;
	private String StudentWriteLength;
	private String WhiteboardSpaceUsage;
	private String TeacherWriteTime;
	private String StudentWriteTime;
	private String WhiteboardInactiveTime;
	private String WhiteBoards;
	private String HWAveragedensity;
	private String HWVariance;
	private String ImageAnnotation;
	private String ActiveDuration;
	private String Interaction;
	private String ProblemsDiscussed;
	private String TeacherTime;
	private String StudentTime;
	private String STRatio;
	private String NoActivity;
	private String SameTime;
	private String Pace;
	private String AudioDuration;
	private String Score;
	private String Quality;

	public GetRequestsResponse(ArrayList<String> temp_edited)
	{
		this._id = temp_edited.get(0);
		this.Subject =  temp_edited.get(1);
		this.ShapesandImages = temp_edited.get(2);
		this.NoImages = temp_edited.get(3);
		this.ColourChanges = temp_edited.get(4);
		this.TeacherWriteLength = temp_edited.get(5);
		this.StudentWriteLength = temp_edited.get(6);
		this.WhiteboardSpaceUsage = temp_edited.get(7);
		this.TeacherWriteTime = temp_edited.get(8);
		this.StudentWriteTime = temp_edited.get(9);
		this.WhiteboardInactiveTime = temp_edited.get(10);
		this.WhiteBoards = temp_edited.get(11);
		this.HWAveragedensity = temp_edited.get(12);
		this.HWVariance = temp_edited.get(13);
		this.ImageAnnotation = temp_edited.get(14);
		this.ActiveDuration = temp_edited.get(15);
		this.Interaction = temp_edited.get(16);
		this.ProblemsDiscussed = temp_edited.get(17);
		this.TeacherTime = temp_edited.get(18);
		this.StudentTime = temp_edited.get(19);
		this.STRatio = temp_edited.get(20);
		this.NoActivity = temp_edited.get(21);
		this.SameTime = temp_edited.get(22);
		this.Pace = temp_edited.get(23);
		this.AudioDuration = temp_edited.get(24);
		this.Score = temp_edited.get(25);
		this.Quality = temp_edited.get(26);
	}
}